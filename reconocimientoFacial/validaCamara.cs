﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using AForge.Video;
using AForge.Video.DirectShow;
using System.Linq;
namespace reconocimientoFacial
{
    public class validaCamara
    {
        private string PathSave = @"G:\imagenesDataSet\";
        private FilterInfoCollection camara;
        private VideoCaptureDevice imageCamara;
       
        public bool validaConexionCamara()
        {
            camara = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            if (camara.Count > 0)
                return true;
            else
                Console.WriteLine("Conecte la camara");
            return false;
        }

        public void guardaImagen()
        {
            string nombreCamara= camara[0].MonikerString;
            imageCamara = new VideoCaptureDevice(nombreCamara);
            imageCamara.NewFrame += new NewFrameEventHandler(capturaImage);
            imageCamara.Start();
            
        }

        public void capturaImage(object sender,NewFrameEventArgs eventArgs)
        {
            var idPersona = generarIdPersona();
            String nombreArchivo = "persona" + idPersona.ToString()+".png";
            Bitmap img = (Bitmap)eventArgs.Frame.Clone();
            img.Save(PathSave + nombreArchivo,ImageFormat.Png);
            cerrarCamara();
            string seachPath = PathSave + nombreArchivo;
            enviaAdvertencia(seachPath);
            Console.WriteLine("verificando......");
        }

        public void cerrarCamara()
        {
            if(imageCamara!=null && imageCamara.IsRunning)
            {
                imageCamara.SignalToStop();
                imageCamara = null;
            }
        }

        public Int64 generarIdPersona()
        {
            Int64 id=0;
            for (int i = 0; i <= 1; i++)
            {
                var guid = Guid.NewGuid();
                var justNumbers = new String(guid.ToString().Where(Char.IsDigit).ToArray());
                var seed = int.Parse(justNumbers.Substring(0, 4));

                var random = new Random(seed);
                var value = random.Next(0, 5);

                id = seed;
            }
            return id;
        }

        private void enviaAdvertencia(String path)
        {
            envioCorreo sender = new envioCorreo();
            sender.enviarCorreoAdvertencia(path);
        }
        
    }
}
